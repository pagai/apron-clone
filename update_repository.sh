#!/usr/bin/env bash

CURRENT_REV_FILE=current_revision.txt
CURRENT_VER_FILE=current_version.txt

echo "Getting apron svn..."

if [ -d apron ]
then
    svn up apron > /dev/null
else
    svn co svn://scm.gforge.inria.fr/svnroot/apron/apron/trunk apron > /dev/null
fi

REV=$(svn info apron | grep "Revision" | cut -d ' ' -f 2)
CURRENT_REV="$(cat "$CURRENT_REV_FILE" 2> /dev/null)"
if [ "$REV" = "$CURRENT_REV" ]
then
    echo " => no update in apron svn: nothing to do"
    exit 0
else
    echo " => updates found in apron svn"
fi

echo "$REV" > "$CURRENT_REV_FILE"

echo "Checking version..."

VER=$(grep "Version" apron/Changes | head -1 | sed -r 's/[^0-9]*([0-9]+(\.[0-9])+)[^0-9]*/\1/')
CURRENT_VER="$(cat "$CURRENT_VER_FILE" 2> /dev/null)"
if [ "$VER" = "$CURRENT_VER" ]
then
    echo " => version hasn't changed: updating archive for version $CURRENT_VER"
    WHATDIDIDO="updated"
else
    echo " => version has changed: creating tarball for version $VER"
    WHATDIDIDO="created"
fi

echo "$VER" > "$CURRENT_VER_FILE"

echo "Creating archive..."

ARCHIVE_PATH="archives"
ARCHIVE_NAME="apron-$VER.tar.xz"
tar cfJ "$ARCHIVE_NAME" apron/ --exclude-vcs # exclude .svn
mv -f "$ARCHIVE_NAME" "$ARCHIVE_PATH/$ARCHIVE_NAME"

echo " => $ARCHIVE_PATH/$ARCHIVE_NAME has been $WHATDIDIDO"

read -p "Autopush to remote repository? (y/n) [y] " ANSWER
if [ -z "$ANSWER" -o "$ANSWER" = "y" -o "$ANSWER" = "Y" ]
then
    echo " => commit/push..."
    git add archives/ "$CURRENT_REV_FILE" "$CURRENT_VER_FILE"
    git commit -m "$WHATDIDIDO archive for version $VER"
    git push origin master
    echo " => all done"
else
    echo " => no autopush: you'll have to do that manually (git commit/push) to make the new archive available"
fi
