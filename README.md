# Apron-clone

This repo is here to create tarballs of [Apron](http://apron.cri.ensmp.fr/library/) and make them available.

Apron uses svn, and for PAGAI we'd like to have a control on which version we use (without requiring end-users to use svn).

This repo does not store apron's code again. We only keep the last version and svn revision number,
as well as the different archives for apron's versions.

## Usage

### Check for Updates

To check new versions of Apron, and create an archive if needed:

    ./update_repository.sh

Then it's done.

### Get an Archive

The archives for apron versions are stored in the `archives` directory.
You can just get the wanted version using e.g.:

    APRON_VERSION=0.9.11
    wget https://gricad-gitlab.univ-grenoble-alpes.fr/pagai/apron-clone/raw/master/archives/apron-$APRON_VERSION.tar.xz

